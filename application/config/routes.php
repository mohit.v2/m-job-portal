<?php
defined('BASEPATH') OR exit('No direct script access allowed');


$route['default_controller'] = 'jobportal';
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;
$route['login']= 'jobportal/login';
$route['home'] = 'jobportal/home';
