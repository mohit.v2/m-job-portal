<div class="cont">

  
    <div class="heading">
      <h2 class="jp">&nbsp;&nbsp;Edit Job Details</h2>
    </div>

    <div style="width:100%; height:10px"></div>
       <div class="furm jp">
       <?php extract($edit) ?>
        <form method="post" action="<?php echo site_url('Employer/updateJob/'.$id); ?>">
        
            <table>
                <tr>
                    <td>Job Title</td>
                    <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                    <td> <input class="emp-inp" type="text" name="job-title" style="width:400px;" value="<?= $job_title  ?>" ></td>
                    <td><br><br></td>
                </tr>
                <tr>
                    <td>Description</td>
                    <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </td>
                    <td><input type="text" name="description" style="width:400px;height:100px;" value="<?= $description  ?>"></td> 
                    <td><br><br><br><br><br><br></td>
                </tr>
                <br><br><br>
                <tr>
                    <td>Budget (CTC)</td>
                    <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                    <td><input type="text" name="budget" style="width:100px;" value="<?= $budget  ?>" >&nbsp;&nbsp;lakhs</td>
                    <td><br><br><br></td>
                </tr>
                <tr>
                    <td>Job Type</td>
                    <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                    <td><select name="job-type" id="job" name="job" >
                        <option class="jp" name="job" value="Full-Time">Full Time</option>
                        <option  class="jp" name="job" value="Part-Time">Part Time</option>
                        <option class="jp" name="job" value="Contract-Based">Contract Based</option> 
                      </select></td>
                    <td><br><br></td>
                </tr>
                <tr>
                    <td>Job Location</td>
                    <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                    <td><select  name="location" id="location">
                        <option class="jp" value="Mumbai">Mumbai</option>
                        <option class="jp" value="Delhi">Delhi</option>
                        <option class="jp" value="Bangalore">Bangalore</option>  
                      </select></td>
                    <td><br><br><br><br></td>
                </tr>
                <tr>
                    <td>Minimum Experience <br>Required</td>
                    <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                    <td><input type="number" name="experience" style="width:100px;" value="<?= $experience  ?>" >&nbsp;&nbsp;Years</td>
                    <td><br><br><br><br><br></td>
                </tr>
                <tr>
                    <td><br><br></td>
                    <td><br><br></td>
                    <td><input class="butn jp" type="submit" value="Update Job"></td>
                    <td><br><br></td>
                </tr>
                

            </table>
            

        </form>
    </div>

    </div>