<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title> | Job Portal</title>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/css/bootstrap.min.css" integrity="sha384-rbsA2VBKQhggwzxH7pPCaAqO46MgnOM80zW1RWuH61DGLwZJEdK2Kadq2F9CUG65" crossorigin="anonymous">
    <link rel="preconnect" href="https://fonts.googleapis.com">
<link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
<link href="https://fonts.googleapis.com/css2?family=Shantell+Sans:wght@300;400&display=swap" rel="stylesheet">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<link rel="stylesheet" href="<?php echo site_url().'assets/css/style.css' ?>">
<script src="https://code.jquery.com/jquery-3.6.4.js" integrity="sha256-a9jBBRygX1Bh5lt8GZjXDzyOB+bWve9EiO7tROUtj/E=" crossorigin="anonymous"></script>
</head>
<body>
<?php  $activePage = basename($_SERVER['PHP_SELF'], ".php"); ?>
    <div class="header">
<div class="logo">
<a style="text-decoration:none;" href="<?php echo site_url().'jobportal/login' ?>"  ><h1 class="jp "> Job Portal </h1></a>
<center>
<?php if($this->session->flashdata('user_loggedin')): ?>
<?php echo '<div class="alert alert-success hide-it" style="width:200px;">'.$this->session->flashdata('user_loggedin').'</div>'; ?>
<?php endif; ?>
</center>
</div>
<div class="navu">
<ul class="nav nav-pills">
  <li class="nav-item">
    <a class="<?= ($activePage == 'dashboard') ? 'active':''; ?> nav-link  jp" aria-current="page" href="<?php echo site_url().'Employer/dashboard' ?>">Dashboard</a>
  </li>
  <li class="nav-item">
    <a class="<?= ($activePage == 'postJob') ? 'active':''; ?> nav-link  jp" href="<?php echo site_url().'Employer/postJob' ?>">Post New Job</a>
  </li>
  <li class="nav-item">
    <a class="<?= ($activePage == 'myJobs') ? 'active':''; ?> nav-link  jp" href="<?php echo site_url().'Employer/myJobs' ?>">My Jobs</a>
  </li>
  <li class="nav-item">
    <a class="<?= ($activePage == 'profile') ? 'active':''; ?> nav-link jp" href="<?php echo site_url().'Employer/profile' ?>" >My Profile</a>
  </li>

  <div style="width:780px" > </div>
    <div class="dropdown " >
        <button style="width:200px;" class="jp" id="comp"><?php echo $this->session->userdata('company'); ?>˅</button>
        <div class="dropdown-content" >
          <a href="<?= site_url().'Employer/logout';?>">Log out</a>
          
        </div>
      </div>
    
</ul>

</div>
 </div>
<br>
<script>
$(document).ready(function(){
  setTimeout(function() {
            $('.hide-it').hide('fast');
        }, 1000);
})
</script>