<div class="cont">
    <div class="heading">
        <h2 class="jp">&nbsp;&nbsp;My Jobs</h2>
      </div>
<br>
<div class="tbl">
<table class="table table-bordered">
  <thead>
    <tr class="top jp">
      <th scope="col">Job Title</th>
      <th scope="col">Job Location</th>
      <th scope="col">Max CTC Budget</th>
      <th scope="col">Applications</th>
      <th scope="col">Actions</th>
    </tr>
  </thead>
  <tbody>
  <?php if($myjobs){ foreach($myjobs as $jobDetail) : ?>
    <tr>
      <td class="jp"><?= $jobDetail['job_title'] ?></th>
      <td class="jp"><?= $jobDetail['job_location'] ?></td>
      <td class="jp"> <?= $jobDetail['budget'] ?> lakhs</td>
      <td class="jp"><?php if($jobDetail['applicants']>0) { ?><a class="jp" href="<?php echo base_url('Employer/viewApplicants/').$jobDetail['job_id'];?>"><?php  echo $jobDetail['applicants']; ?> </a> <?php } else{ echo $jobDetail['applicants'];} ?> </td>
      <td class="jp"><a class="jp" href="<?php echo base_url('Employer/editJob/').$jobDetail['id'];?>">Edit</a> | <a class="jp" href="<?php echo site_url('Employer/deleteJob/'.$jobDetail['id']); ?>">Delete</a></td>
    </tr>
    <?php endforeach;  } else { if($this->session->flashdata('no_data')) : ?>
  <?php echo '<p class="alert alert-danger">'.$this->session->flashdata('no_data').'</p>'; ?>
  <?php endif; }?>

  </tbody>
</table>

</div>



  

</div>
