<div class="cont">

<?php extract($applicant);   ?>

    <div class="heading">
        <h2 class="jp">&nbsp;&nbsp;All Applicants For <?= $applicant[0]['jtitle'];  ?>
 </h2>
      </div>
<br>
<div class="tbl">
<table class="table table-bordered">
  <thead>
    <tr class="top jp">
      <th scope="col">Name</th>
      <th scope="col">Title</th>
      <th scope="col">Current CTC (Lakhs)</th>
      <th scope="col">Current Location</th>
      <th scope="col">Actions</th>
    </tr>
  </thead>
  <tbody>
  <?php foreach($applicant as $value) {  ?>
    <tr>
      <td class="jp"><?= $value['firstname']." ". $value['lastname'] ; ?></th>
      <td class="jp"><?=  $value['job_title'] ; ?></td>
      <td class="jp"><?= $value['current_ctc'] ?></td>
      <td class="jp"><?= $value['current_location'] ?></td>
      <td class="jp"><a href="<?php echo site_url('employer/download_resume/'.$value['id']) ; ?>">Download Resume</a></td>
    </tr>
    <?php  };  ?>
  </tbody>
</table>
</div>
</div>