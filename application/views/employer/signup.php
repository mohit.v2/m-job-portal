<div class="cont">

  
    <div class="heading">
      <h2 class="jp">&nbsp;&nbsp;Employer Sign Up</h2>
    </div>

    <div class="stick">
        <img class="img" src="<?php echo site_url().'assets/images/emp.png' ?>"> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
         <button href="#" class="butn jp" >I'm an Employer</button>
       </div>
       <div class="furm jp">
        <form method="post" action="<?php echo site_url('Employer/signup'); ?>">
            <table>
                <tr>
                    <td>Company Name<small class="req">*</small></td>
                    <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </td>
                    <td> <input class="emp-inp" type="text" name="company-name" style="width:400px;" value="<?php echo set_value('company-name');?>" ></td>
                    <td><br><br></td>
                </tr>
                <tr>
                  <td></td><td></td>
                 <td> <h6 style="color:red"><?php echo form_error('company-name'); ?></h6></td>
                </tr>
                <tr>
                <tr>
                    <td>Company Address<small class="req">*</small></td>
                    <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </td>
                    <td><input type="text" name="company-address" style="width:400px;height:100px;" value="<?php echo set_value('company-address');?>"></td> 
                    <td><br><br><br><br><br></td>
                </tr>
                <tr>
                  <td></td><td></td>
                 <td> <h6 style="color:red"><?php echo form_error('company-address'); ?></h6></td>
                </tr>
                <tr>
                    <td>First Name<small class="req">*</small></td>
                    <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                    <td><input class="emp-inp" type="text" name="firstname" style="width:400px;" value="<?php echo set_value('firstname');?>"></td>
                    <td><br><br></td>
                </tr>
                <tr>
                  <td></td><td></td>
                 <td> <h6 style="color:red"><?php echo form_error('firstname'); ?></h6></td>
                </tr>
                <tr>
                    <td>Last Name<small class="req">*</small></td>
                    <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                    <td><input class="emp-inp" type="text" name="lastname"style="width:400px;" value="<?php echo set_value('lastname');?>"></td>
                    <td><br><br></td>
                </tr>
                <tr>
                  <td></td><td></td>
                 <td> <h6 style="color:red"><?php echo form_error('lastname'); ?></h6></td>
                </tr>
                <tr>
                    <td>Gender</td>
                    <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                    <td><input type="radio" id="male" name="gender" value="Male">
                        <label for="html" class="jp">Male</label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        <input type="radio" id="female" name="gender" value="Female">
                        <label for="html" class="jp">Female</label></td>
                    <td><br><br></td>
                </tr>
                <tr>
                    <td>Contact Number<small class="req">*</small></td>
                    <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                    <td><input class="emp-inp" type="text" name="contact" style="width:400px;" value="<?php echo set_value('contact');?>"></td>
                    <td><br><br></td>
                </tr>
                <tr>
                <td></td><td></td>
                  <td> <h6 style="color:red"><?php echo form_error('contact'); ?></h6></td>
                 </tr>
                <tr>
                    <td>Current Location
                    <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                    <td><input class="emp-inp" type="text" name="location" style="width:400px;" value="<?php echo set_value('location');?>"></td>
                    <td><br><br></td>
                </tr>
                <tr>
                    <td>Email Id<small class="req">*</small></td>
                    <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                    <td><input class="emp-inp" type="text" name="email" style="width:400px;" value="<?php echo set_value('email');?>"></td>
                    <td><br><br></td>
                </tr>
                <tr>
                  <td></td><td></td>
                 <td> <h6 style="color:red"><?php echo form_error('email'); ?></h6></td>
                </tr>
                <tr>
                    <td>Password<small class="req">*</small></td>
                    <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                    <td><input class="emp-inp" type="text" name="password" style="width:400px;" value="<?php echo set_value('password');?>" ></td>
                    <td><br><br></td>
                </tr>
                <tr>
                  <td></td><td></td>
                 <td> <h6 style="color:red"><?php echo form_error('password'); ?></h6></td>
                </tr>
                <tr>
                    <td>Re-enter Password<small class="req">*</small></td>
                    <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                    <td><input class="emp-inp" type="text" name="passconf" style="width:400px;"></td>
                    <td><br><br></td>
                </tr>
                <tr>
                  <td></td><td></td>
                 <td> <h6 style="color:red"><?php echo form_error('passconf'); ?></h6></td>
                </tr>
                <tr>
                    <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                    <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                    <td><input type="checkbox" id="agree" name="checkbox" value="agree">&nbsp;&nbsp;<label for="agree" class="jp"> I Agree to the Terms & Conditions</label></td>
                    <td><br><br></td>
                </tr>
                <tr>
                  <td></td><td></td>
                 <td> <h6 style="color:red"><?php echo form_error('checkbox'); ?></h6></td>
                </tr>
                <tr>
                    <td><br><br></td>
                    <td><br><br></td>
                    <td><input class="butn jp" type="submit" value="Sign Up"></td>
                    <td><br><br></td>
                  
                </tr>
            </table>         
        </form>    
    </div>
    <div style="width:100%; height:30px"></div>
    </div>
