<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title> | Job Portal</title>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/css/bootstrap.min.css" integrity="sha384-rbsA2VBKQhggwzxH7pPCaAqO46MgnOM80zW1RWuH61DGLwZJEdK2Kadq2F9CUG65" crossorigin="anonymous">
    <link rel="preconnect" href="https://fonts.googleapis.com">
<link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
<link href="https://fonts.googleapis.com/css2?family=Shantell+Sans:wght@300;400&display=swap" rel="stylesheet">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<link rel="stylesheet" href="//cdn.datatables.net/1.13.4/css/jquery.dataTables.min.css" > 
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.5.2/css/bootstrap.css">
<link rel="stylesheet" href="https://cdn.datatables.net/1.13.4/css/dataTables.bootstrap4.min.css">
<script src = "//cdn.datatables.net/1.13.4/js/jquery.dataTables.min.js"></script>
<script src = "https://cdn.datatables.net/1.13.4/js/dataTables.bootstrap4.min.js"></script>
<link rel="stylesheet" href="<?php echo base_url().'assets/css/style.css'; ?>" type="text/css">
<script src="https://code.jquery.com/jquery-3.6.4.js" integrity="sha256-a9jBBRygX1Bh5lt8GZjXDzyOB+bWve9EiO7tROUtj/E=" crossorigin="anonymous"></script>
</head>
<body>
<?php  $activePage = basename($_SERVER['PHP_SELF'], ".php"); ?>
    <div class="header">
<div class="logo">
<a style="text-decoration:none;" href="<?php echo site_url().'jobportal/login' ?>"  ><h1 class="jp "> Job Portal </h1></a>
<center>
<?php if($this->session->flashdata('user_loggedin')): ?>
<?php echo '<div class="alert alert-success hide-it" style="width:200px;">'.$this->session->flashdata('user_loggedin').'</div>'; ?>
<?php endif; ?>
</center>
</div>
<div class="navu">
<ul class="nav nav-pills">
  <li class="nav-item">
    <a class="<?= ($activePage == 'dashboard') ? 'active':''; ?> nav-link  jp" aria-current="page" href="<?php echo site_url().'Candidate/dashboard' ?>">Dashboard</a>
  </li>
  <li class="nav-item">
    <a class="<?= ($activePage == 'searchJobs') ? 'active':''; ?> nav-link  jp" href="<?php echo site_url().'Candidate/searchJobs' ?>">Search Jobs</a>
  </li>
  <li class="nav-item">
    <a class="<?= ($activePage == 'appliedJobDetails') ? 'active':''; ?> nav-link  jp" href="<?php echo site_url().'Candidate/appliedJobDetails' ?>">My Applied Jobs</a>
  </li>
  <li class="nav-item">
    <a class="<?= ($activePage == 'profile') ? 'active':''; ?> nav-link jp" href="<?php echo site_url().'Candidate/profile' ?>" >My Profile</a>
  </li>
  
  <div style="width:700px;"></div>
  <li>
    <div class="dropdown " >
        <button style="width:200px;" class=" nav-link mx-auto jp form-inline my-2 my-sm-0"><?php echo $this->session->userdata('username'); ?>˅</button>
        <div class="dropdown-content" style="left:0;">
          <a href="<?= site_url();?>Candidate/logout">Log out</a>
          
        </div>
      </div>
    </li>


</ul>

</div>
 </div>
<br>

<script>
$(document).ready(function(){
  setTimeout(function() {
            $('.hide-it').hide('fast');
        }, 1000);
})
</script>