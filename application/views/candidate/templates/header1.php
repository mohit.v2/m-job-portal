<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>| Job Portal</title>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/css/bootstrap.min.css" integrity="sha384-rbsA2VBKQhggwzxH7pPCaAqO46MgnOM80zW1RWuH61DGLwZJEdK2Kadq2F9CUG65" crossorigin="anonymous">
    <link rel="preconnect" href="https://fonts.googleapis.com">
<link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
<link href="https://fonts.googleapis.com/css2?family=Shantell+Sans:wght@300;400&display=swap" rel="stylesheet">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/js/bootstrap.min.js" integrity="sha384-cuYeSxntonz0PPNlHhBs68uyIAVpIIOZZ5JqeqvYYIcEL727kskC66kF92t6Xl2V" crossorigin="anonymous"></script>
<link rel="stylesheet" href="<?php echo site_url().'assets/css/style.css' ?>">
<script src="https://code.jquery.com/jquery-3.6.4.js" integrity="sha256-a9jBBRygX1Bh5lt8GZjXDzyOB+bWve9EiO7tROUtj/E=" crossorigin="anonymous"></script>  
</head>
<body>
  <?php  $activePage = basename($_SERVER['PHP_SELF'], ".php"); ?>
    <div class="header">
<div class="logo">
<a style="text-decoration:none;" href="<?php echo site_url().'jobportal/login' ?>"  ><h1 class="jp"> Job Portal </h1></a>
</div>

<div class="navu">
<ul class="nav nav-pills">
  <li class=" nav-item">
    <a class="<?= ($activePage == 'home') ? 'active':''; ?> nav-link jp" aria-current="page" href="<?php echo site_url().'jobportal/home' ?>">Home</a>
  </li>
  <li class="nav-item">
    <a class=" <?= ($activePage == 'login') ? 'active':''; ?>  nav-link  jp" href="<?php echo site_url().'Candidate/login' ?>">Log In</a>
  </li>
  <li class="nav-item">
    <a class="<?= ($activePage == 'signup') ? 'active':''; ?>  nav-link jp" href="<?php echo site_url().'Candidate/signup' ?>">Sign Up</a>
  </li>
  <li class="nav-item">
    <a class="<?= ($activePage == 'contactUs') ? 'active':''; ?>  nav-link jp " href="<?php echo site_url().'Candidate/contactUs' ?>">Contact Us</a>
  </li>
</ul>

</div>
 </div><br>

 