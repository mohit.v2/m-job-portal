<div class="cont1">

    <div class="heading">
        <h2 class="jp">&nbsp;&nbsp;Job Search</h2>
      </div>

<br>
<div class="tbl1">
<form method="post" action="<?php echo site_url('Candidate/search')  ?>">
<div class="search">

    <div class="c1">
<h4 class="jp">&nbsp;&nbsp;Job Type:</h4><br>
<select name="job-type" id="job" style="width: 250px;">
    <option class="jp" value="full-time">Full Time</option>
    <option  class="jp" value="part-time">Part Time</option>
    <option class="jp" value="contract-based">Contract Based</option> 
  </select>
     </div>
<div class="c2">
  <h4 class="jp">&nbsp;&nbsp;Job Location:</h4><br>
<select  name="job-location" id="job" style="width: 250px;">
    
    <option class="jp" value="Mumbai">Mumbai</option>
    <option class="jp" value="Delhi">Delhi</option>
    <option class="jp" value="Bangalore">Bangalore</option>
  </select>
</div>
<div class="c3">
  <button type="submit" value="Search" class="btn btn-primary jp">Search</button>
</div>

</div>
</div>


</div>

<br>



<div class="cont">

    <h2 class="jp" style="padding-top:20px;">&nbsp;&nbsp;Search Results</h2>
<br>
<div class="tbl" >
    <table class="table table-bordered" id="jobsTbl">
        <thead>
          <tr class="top jp">
            <th scope="col">Job Title</th>
            <th scope="col">Company Name</th>
            <th scope="col">Job Location</th>
            
          </tr>
        </thead>
        <tbody>
        <?php
      foreach($job as $jobcolumn){
  ?>
          <tr>
           <td> <a href="<?php echo site_url('Candidate/jobDetails/'.$jobcolumn['id'])?>" class="jp"><?= $jobcolumn['job_title']; ?></a></td>
            <td class='jp'><?= $jobcolumn['company_name']; ?></td>
            <td class='jp'><?= $jobcolumn['job_location']; ?></td>
          </tr>
          <?php  } ?>
        </tbody>
    </table> 
</div>
      </form>
</div>


<script>
$(document).ready(function(){
  let table = new DataTable('#jobsTbl');
});

</script>