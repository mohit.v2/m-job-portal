<div class="cont">

  <section class="vh-100 ">
    <div class="heading">
      <h2 class="jp">&nbsp;&nbsp;<?php echo $title ?> </h2>
    </div>
    
      
      <div class="row d-flex justify-content-center align-items-center h-75">
        <div class="col-8 col-md-4 col-lg-4 col-xl-3">
          <div class="card shadow-3-strong" >
            <div class="card-body p-5 text-center">
                <?php if($this->session->flashdata('user_loggedout')) : ?>
  <?php echo '<p class="alert alert-danger hide-it">'.$this->session->flashdata('user_loggedout').'</p>'; ?>
  <?php endif; ?>
  <?php if($this->session->flashdata('login_failed')) : ?>
    <div class="alert alert-danger hide-it">
    <?php echo $this->session->flashdata('login_failed') ?>
    </div> 
    <?php endif; ?>
    <?php if($this->session->flashdata('registered')) : ?>
    <div class="alert alert-success hide-it">
    <?php echo $this->session->flashdata('registered') ?>
    </div> 
    <?php endif; ?>
            <form method="post" action="<?php echo site_url('Candidate/login'); ?>">
              <div class="form-outline form-black mb-4">
                <label class="form-label jp" for="typeEmailX-2">Username</label><br><br>
                <input type="email" id="user" name="email" class="form-control form-control-lg" />
                <span style="color:red;"><?php echo form_error('email'); ?></span>
               
              </div>
  
              <div class="form-outline mb-4">
                <label class="form-label jp" for="typePasswordX-2">Password</label><br><br>
                <input type="password" id="pass" name="password" class="form-control form-control-lg" />
                <span style="color:red;"><?php echo form_error('password'); ?></span>
              </div>
  
              
  
              <button class="btn btn-secondary btn-lg btn-block jp" type="submit">Login</button>
              <br><br><a class="btn btn-secondary btn-lg btn-block jp" href="<?php echo site_url().'Candidate/sgnup' ?>">Sign Up</a>
  
</form>
  
            </div>
          </div>
        </div>
      </div>
    
  </section>

</div>



<script>
$(document).ready(function(){
  setTimeout(function() {
            $('.hide-it').hide('fast');
        }, 2000);
})
</script>