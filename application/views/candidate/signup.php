<div class="cont">


    <div class="heading">
      <h2 class="jp">&nbsp;&nbsp;&nbsp;Candidate Sign Up</h2>
    </div>
    <br><br><br>
    <div class="sticky1">
      <img class="img" src="<?php echo base_url().'assets/images/ind.png' ?>"> 
       <button href="#" class="butn jp" >I'm a Candidate</button>
     </div>
              <br><br><br>
     <div class="furm jp">
        <form method="post" action="<?php echo site_url('Candidate/signup'); ?>">
        <?php if($this->session->flashdata('register_fail')) : ?>
    <div class="alert alert-danger">
    <?php echo $this->session->flashdata('register_fail') ?>
    </div> 
    <?php endif; ?>
            <table>
                <tr>
                    <td>Current CTC</td>
                    <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </td>
                    <td> <input value="<?php echo set_value('ctc');?>" class="emp-inp" type="text" name="ctc" >&nbsp;&nbsp;&nbsp;lakhs</td>
                    <td><br><br></td>
                </tr>
                
                <tr>
                    <td>Job Title<small class="req">*</small></td>
                    <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </td>
                    <td><input type="text"  name="job_title"  class="emp-inp" value="<?php echo set_value('job_title');?>"></td> 
                    <td></td>
                </tr>
                <tr>
                  <td></td><td></td>
                 <td> <h6 style="color:red"><?php echo form_error('job_title'); ?></h6></td>
                </tr>
                <tr>
                    <td>Resume<small class="req">*</small></td>
                    <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                    <td><input class="emp-inp" type="file" name="resume" ></td>
                    <td><br><br></td>
                </tr>
                <tr>
                <td></td><td></td>
                 <td> <h6 style="color:red"><?php echo form_error('resume'); ?></h6></td>
                </tr>
                <tr>
                    <td>First Name<small class="req">*</small></td>
                    <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                    <td><input class="emp-inp" type="text" name="firstName" value="<?php echo set_value('firstName');?>"></td>
                    <td><br><br></td>
                </tr>
                <tr>
                <td></td><td></td>
                 <td> <h6 style="color:red"><?php echo form_error('firstName'); ?></h6></td>
                </tr>
                <tr>
                  <td>Last Name<small class="req">*</small></td>
                  <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                  <td><input class="emp-inp" type="text" name="lastname" value="<?php echo set_value('lastname');?>" ></td>
                  <td><br><br></td>
              </tr>
              <tr>
              <td></td><td></td>
               <td> <h6 style="color:red"><?php echo form_error('lastname'); ?></h6></td>
              </tr>
                <tr>
                    <td>Gender</td>
                    <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                    <td><input type="radio" id="male" name="gender" value="Male">
                        <label for="html" class="jp">Male</label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        <input type="radio" id="female" name="gender" value="Female">
                        <label for="html" class="jp">Female</label></td>
                    <td><br><br></td>
                </tr>
                <tr>
                    <td>Date of Birth</td>
                    <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                    <td><input class="emp-inp" type="date" name="dob" value="<?php echo set_value('dob');?>"></td>
                    <td><br><br></td>
                </tr>
                <tr>
                    <td>Contact Number<small class="req">*</small></td>
                    <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                    <td><input class="emp-inp" type="text" name="contact" value="<?php echo set_value('contact');?>" ></td>
                    <td><br><br></td>
                </tr>
                <tr>
                <td></td><td></td>
                  <td> <h6 style="color:red"><?php echo form_error('contact'); ?></h6></td>
                 </tr>
                <tr>
                    <td>Email Id<small class="req">*</small></td>
                    <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                    <td><input class="emp-inp" type="text" name="email" value="<?php echo set_value('email');?>"></td>
                    <td><br><br></td>
                </tr>
                <tr>
                <td></td><td></td>
                  <td> <h6 style="color:red"><?php echo form_error('email'); ?></h6></td>
                 </tr>
                <tr>
                    <td>Password<small class="req">*</small></td>
                    <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                    <td><input class="emp-inp" type="password" name="password" value="<?php echo set_value('password');?>"></td>
                    <td><br><br></td>
                </tr>
                <tr>
                <td></td><td></td>
                  <td> <h6 style="color:red"><?php echo form_error('password'); ?></h6></td>
                 </tr>
                <tr>
                    <td>Re-enter Password<small class="req">*</small></td>
                    <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                    <td><input class="emp-inp" type="password" name="passconf" ></td>
                    <td><br><br></td>
                </tr>
                <tr>
                <td></td><td></td>
                  <td> <h6 style="color:red"><?php echo form_error('passconf'); ?></h6></td>
                 </tr>
                <tr>
                  <td>Profile Picture<small class="req">*</small></td>
                  <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                  <td><input class="emp-inp" type="file" name="profile_pic" ></td>
                  <td><br><br></td>
              </tr>
              <tr>
              <td></td><td></td>
                <td> <h6 style="color:red"><?php echo form_error('profile_pic'); ?></h6></td>
               </tr>
              <tr>
                <td>Experience (in years)</td>
                <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                <td><input class="emp-inp" type="text" name="experience" value="<?php echo set_value('experience');?>"></td>
                <td><br><br></td>
            </tr>
            <tr>
              <td>Current Location</td>
              <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
              <td><input class="emp-inp" type="text" name="location" value="<?php echo set_value('location');?>"></td>
              <td><br><br></td>
          </tr>
                <tr>
                    <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                    <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                    <td><input type="checkbox" id="agree" name="checkbox" value="checkbox">&nbsp;&nbsp;<label for="agree" class="jp"> I Agree to the Terms & Conditions</label></td>
                    <td><br><br></td>
                </tr>
                
               
               <tr>
               <td></td><td></td>
                <td><h6 style="color:red"><?php echo form_error('checkbox'); ?></h6></td>
              </tr>
                <tr>
                    <td><br><br></td>
                    <td><br><br></td>
                    <td><input class="butn jp" type="submit" value="Sign Up"></td>
                    <td><br><br></td>
                </tr>


            </table>
            

        </form>
</div>
    
  
</div>
