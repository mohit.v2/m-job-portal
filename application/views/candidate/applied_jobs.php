<div class="cont">

  

    <div class="heading">
        <h2 class="jp">&nbsp;&nbsp;My Applied Jobs</h2>
      </div>

<br>
<div class="tbl">
<table class="table table-bordered">
  <thead>
    <tr class="top jp">
      <th scope="col">Title</th>
      <th scope="col">Company Name</th>
      <th scope="col">Current Location</th>
      <th scope="col">Min. Experience Required</th>
      <th scope="col">Applied on</th>
    </tr>
  </thead>
  <tbody>
    <?php foreach($appliedjob as $job) : ?>

<?php 

$dateTime = new DateTime($job['applied_on']);
$unix =  $dateTime->format('U');
$now = time();




?>


    <tr class="jp">
      <td><a href="<?php echo site_url().'candidate/withdrawApplication/'.$job['id'] ?>" class="jp"><?= $job['job_title']; ?></a></th>
      <td><?= $job['company_name']; ?></td>
      <td><?= $job['current_location']; ?></td>
      <td><?= $job['experience']; ?></td>
      <td><?= timespan($unix, $now) . ' ago';   ?></td>
    </tr>
   <?php endforeach; ?>
    
  </tbody>
</table>

</div>



  

</div>
