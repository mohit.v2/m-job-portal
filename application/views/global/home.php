<div class="cont">

  

            <div class="heading">
                <h2 class="jp">&nbsp;&nbsp;Home</h2>
              </div>

              <div id="carouselExampleDark" class="carousel carousel-dark slide">
  <div class="carousel-indicators">
    <button type="button" data-bs-target="#carouselExampleDark" data-bs-slide-to="0" class="active" aria-current="true" aria-label="Slide 1"></button>
    <button type="button" data-bs-target="#carouselExampleDark" data-bs-slide-to="1" aria-label="Slide 2"></button>
    <button type="button" data-bs-target="#carouselExampleDark" data-bs-slide-to="2" aria-label="Slide 3"></button>
  </div>
  <div class="carousel-inner">
    <div class="carousel-item active" data-bs-interval="10000">
      <img src="<?php echo base_url().'assets/images/b.jpg' ?>" class="d-block w-100" alt="...">
      <div class="carousel-caption d-none d-md-block">
        <h1 class="jp" style="color:black; ">Start your Career Journey Today!</h5>
        <h4 class="jp" style="color:white">Your Job Search Ends Here!</h4>
      </div>
    </div>
    <div class="carousel-item" data-bs-interval="2000">
      <img src="<?php echo base_url().'assets/images/a.jpeg' ?>" class="d-block w-100" alt="...">
      <div class="carousel-caption d-none d-md-block">
        <h1 class="jp" style="color:white">All Fortune 500 Companies</h1>
        <h4 class="jp" style="color:white">Build your Network</h4>
      </div>
    </div>
    <div class="carousel-item">
      <img src="<?php echo base_url().'assets/images/d.jpg' ?>" class="d-block w-100" alt="...">
      <div class="carousel-caption d-none d-md-block">
        <h1 class="jp" style="color:white">Apply Today & Get Placed</h1>
        <h5 class="jp" style="color:white">Register Now!</h5>
      </div>
    </div>
  </div>
  <button class="carousel-control-prev" type="button" data-bs-target="#carouselExampleDark" data-bs-slide="prev">
    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
    <span class="visually-hidden">Previous</span>
  </button>
  <button class="carousel-control-next" type="button" data-bs-target="#carouselExampleDark" data-bs-slide="next">
    <span class="carousel-control-next-icon" aria-hidden="true"></span>
    <span class="visually-hidden">Next</span>
  </button>
</div>
         

        </div>

</div>