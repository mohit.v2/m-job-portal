<?php
class CandidateModel extends CI_Model
{
    public function insertCandidate($data)
    {
        $this->db->insert('candidates',$data);
        return true;
    }

    public function check_email_exists($email)
    {
        $query = $this->db->get_where('candidates',array('email_id' => $email));
        if(empty($query->row_array()))
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    public function check_contact_exists($contact)
    {
        $query = $this->db->get_where('candidates',array('contact_no' => $contact));
        if(empty($query->row_array()))
        {
            return true;
        }
        else
        {
            return false;
        }
    }
    
    public function loginCandidate($email,$password)
    {
        $id = $this->db->select('id')
        ->where('email_id',$email)
	    ->where('password', $password)
        -> limit(1)
        ->get('candidates')
        ->row()->id;
        if($id)
        {
            return $id;
        }
        else
        {
            return false;
        }
    }
 
    public function get_profile($id)
    {
        $this->db->select('*');
        $this->db->where('id', $id);
        $row = $this->db->get('candidates');
        return $row->row_array();
    }

    public function update($data,$id)
    {
        $this->db->where('id',$id);
        $this->db->update('candidates',$data);
        return $data;
    }

    public function insertAppliedJobs($candidate_id,$job_id,$data)
    {
        $this->db->where('job_id',$job_id);
        $this->db->where('candidate_id',$candidate_id);
        $q = $this->db->get('applied_jobs');
        if ( $q->num_rows() > 0 ) 
        {
            return false;
        } else {
            $this->db->insert('applied_jobs',$data);
            return true;
        }
    }

    public function displayAppliedJobs($candidate_id)
    {
        $query = ("SELECT applied_jobs.*, jobs.*
        FROM applied_jobs
        INNER JOIN jobs on applied_jobs.job_id = jobs.id
        WHERE applied_jobs.candidate_id = '".$candidate_id."' ");
        return $this->db->query($query)->result_array();
    }

    public function get_applicants($job_id)
    {
      $query = ("SELECT candidates.*,
      applied_jobs.id,applied_jobs.job_id,applied_jobs.candidate_id,applied_jobs.job_id,
      jobs.id, jobs.employer_id, jobs.job_location,jobs.job_title As jtitle
      FROM candidates
      INNER JOIN applied_jobs ON candidates.id = applied_jobs.candidate_id 
      INNER JOIN jobs ON jobs.id = applied_jobs.job_id  
     WHERE jobs.id = '".$job_id."'
     ");
      return $this->db->query($query)->result_array();  
    }

    public function deleteAppliedJob($job_id,$candidate_id)
    {
        $this->db->where('job_id', $job_id);
        $this->db->where('candidate_id', $candidate_id);
        $this->db->delete('applied_jobs');
        return true; 
    }
}  

