<?php

class EmployerModel extends CI_Model{
    
    public function InsertemployerData($data)
    {
        $this->db->insert('employers',$data);
        return true;
    }

    public function check_email_exists($email)
    {
        $query = $this->db->get_where('candidates',array('email_id' => $email));
        if(empty($query->row_array())){
            return true;
        }
        else
        {
            return false;
        }
    }
    
    public function check_contact_exists($contact)
    {
        $query = $this->db->get_where('candidates',array('contact_no' => $contact));
        if(empty($query->row_array()))
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    public function employerLogin($email,$password)
    {
        $id = $this->db->select('id')
        ->where('email_id',$email)
	    ->where('password', $password)
        -> limit(1)
        ->get('employers')
        ->row()->id;
        if($id)
        {
            return $id;
        }
        else
        {
            return false;
        }
    }

    public function get_profile($id)
    { 
        $this->db->select('*');
        $this->db->where('id', $id);
        $row = $this->db->get('employers');
        return $row->row_array();
    }

    public function updateEmployer($data,$id)
    {
        $this->db->where('id',$id);
        $this->db->update('employers',$data);
        return $data;
    }

    public function insertJobdata($data)
    {
        $this->db->insert('jobs',$data);
        return $data;
    }

    public function displayJobs()
    {
        $this->load->database();
        $query = $this->db->query("select * from jobs");
        return $query->result_array();
    }    
      
    public function search_job($jobtype, $location)
    {
        $query = ("SELECT employers.*, jobs.*
        FROM jobs
        INNER JOIN employers on employers.id = jobs.employer_id
        WHERE jobs.job_type = '".$jobtype."' AND jobs.job_location = '".$location."'");
        return $this->db->query($query)->result_array();
    }

    public function displayOnSearch()
    {
        $query = ("SELECT employers.*, jobs.*
        FROM jobs
        INNER JOIN employers on employers.id = jobs.employer_id");
        return $this->db->query($query)->result_array();
    }

    public function editJob($id)
    {
        $this->db->select('*');
        $this->db->where('id', $id);
        $row = $this->db->get('jobs');
        return $row->row_array();
    }

    public function deleteJob($id)
    {
        $this->db->where('id', $id);
        $this->db->delete('jobs');
        return true; 
    }

    public function updateJob($id,$data)
    {
        $this->db->where('id', $id);
        $this->db->update('jobs', $data);
    }

    public function getJobById($id)
    {
        $this->db->select('*');
        $this->db->from('employers');
        $this->db->join('jobs', 'jobs.employer_id = employers.id');
        $this->db->where('jobs.id', $id);
        $query = $this->db->get()->row_array();
        return $query;
    }    

    public function myJobs($employer_id)
    { 
        $this->db->select('applied_jobs.id,applied_jobs.job_id,applied_jobs.candidate_id,count(applied_jobs.job_id) As applicants ');
        $this->db->select('jobs.id,jobs.employer_id,jobs.job_title,jobs.budget,jobs.job_location');
        $this->db->from('applied_jobs');
        $this->db->join('jobs','applied_jobs.job_id = jobs.id', 'right');
        $this->db->where('jobs.employer_id', $employer_id);
        $this->db->group_by('jobs.id');
        $query = $this->db->get()->result_array();
        if ($query){
            return $query;
        }else{
            return false;
        }
    }

    public function companyName($id)
    {
        $company = $this->db->select('company_name')
        ->where('id',$id)
        ->limit(1)
        ->get('employers')
        ->row()->company_name;

        if($company)
        {
            return $company;
        }
        else
        {
            return false;
        }
    }

    public function get_resume($id)
    {
        $this->db->where('id', $id);
        $this->db->select('resume');
        $query=$this->db->get('candidates');
        return $query->result_array();      
    }

    public function download_picture($id)
    {
        $this->db->where('id', $id);
        $this->db->select('profile_pic');
        $query=$this->db->get('candidates');
        return $query->result_array();  
    }
}

