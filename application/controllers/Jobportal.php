<?php

class Jobportal extends CI_Controller
{
    public function index()
    {
        $data['title']= "Login";
        $this->load->view('global/templates/header',$data);
        $this->load->view('global/login');
        $this->load->view('global/templates/footer');
    }



    public function register()
    {
        $data['title']= "Sign Up";
        $this->load->view('global/templates/header',$data);
        $this->load->view('global/signup');
        $this->load->view('global/templates/footer');
    }

    public function login()
    {
        $data['title']= "Log In";
        $this->load->view('global/templates/header',$data);
        $this->load->view('global/login');
        $this->load->view('global/templates/footer');
    }

    public function home()
    {
        $data['title']= "Home";
        $this->load->view('global/templates/header',$data);
        $this->load->view('global/home');
        $this->load->view('global/templates/footer');
    }

    public function contactUs()
    {
        $data['title']= "Contact Us";
        $this->load->view('global/templates/header',$data);
        $this->load->view('global/contactus');
        $this->load->view('global/templates/footer');
    }
}