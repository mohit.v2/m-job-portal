<?php 

class Employer extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->database();
        $this->load->model('EmployerModel');
        $this->load->model('CandidateModel');
    }

    public function index()
    {
        $this->load->view('employer/templates/header1');
        $this->load->view('employer/login');
        $this->load->view('employer/templates/footer0');
    }

    public function login()
    {
        if($this->session->userdata('id')){
        redirect('employer/dashboard');exit;}
        $data['title'] = 'Employer Log In';
        $this->form_validation->set_rules('email','Email ID','trim|required|valid_email');
        $this->form_validation->set_rules('password','Password','required');
        $this->form_validation->set_error_delimiters('<div class="error">', '</div>');
        if($this->form_validation->run() === FALSE)
        {
            $this->load->view('employer/templates/header1',$data);
            $this->load->view('employer/login');
            $this->load->view('employer/templates/footer0');
        }
        else
        {
            $email = $this->input->post('email');
            $password = md5($this->input->post('password'));
            $employer_id	=$this->EmployerModel->employerLogin($email,$password);
            $company_name = $this->EmployerModel->companyName($employer_id);
            if($employer_id)
            {
                $employer  	=array(
                'id'        	=>$employer_id,
                'company'       =>$company_name);
                $this->session->set_userdata($employer);
                $this->session->set_flashdata('user_loggedin','You are now logged in');
                redirect('employer/dashboard',$employer);exit;
            }
            else
            {
                $this->session->set_flashdata('login_failed','Incorrect Username or Password');
                redirect('employer/login');exit;
            }     
        }
    }

    public function dashboard()
    {
        if(!$this->session->userdata('id')){
        redirect('employer/login');exit;}
        $data['title']="Dashboard";
        $this->load->view('employer/templates/header2',$data);
        $this->load->view('employer/dashboard');
        $this->load->view('employer/templates/footer1');
    }

    public function check_email_exists($email)
    {
        $this->form_validation->set_message('check_email_exists','That email is taken please choose a different one');
        if( $this->EmployerModel->check_email_exists($email))
        {
            return true;
        } 
        else
        {
            return false;
        }
    }

    public function check_contact_exists($contact)
    {
        $this->form_validation->set_message('check_contact_exists','Phone number already exists, please login or choose a different number for registraion');
        if( $this->CandidateModel->check_contact_exists($contact))
        {
            return true;
        }
        else
        {
            return false;
        }
    }
    
    public function signup()
    {
        $this->form_validation->set_rules('firstname','First Name','required');
        $this->form_validation->set_rules('email','Email Id','trim|required|valid_email|callback_check_email_exists');
        $this->form_validation->set_rules('lastname','Last name','required');
        $this->form_validation->set_rules('password','Password','required');
        $this->form_validation->set_rules('passconf', 'Password Confirmation', 'required|matches[password]');
        $this->form_validation->set_rules('company-name','Company Name','required');
        $this->form_validation->set_rules('checkbox','terms and conditions','required');
        $this->form_validation->set_rules('company-address','company address','required');
        $this->form_validation->set_rules('contact','Contact No','required|callback_check_contact_exists');
        if($this->form_validation->run()=== FALSE)
        {
            $this->load->view('employer/templates/header1');
            $this->load->view('employer/signup');
            $this->load->view('employer/templates/footer1'); 
        }
        else
        {
            $data['company_name']		=$this->input->post('company-name');
            $data['company_address']	=$this->input->post('company-address');
            $data['firstname']			=$this->input->post('firstname');
            $data['lastname']			=$this->input->post('lastname');
            $data['gender']				=$this->input->post('gender');
            $data['contact_no']			=$this->input->post('contact');
            $data['current_location']	=$this->input->post('location');
            $data['email_id']			=$this->input->post('email');
            $data['password']		    =md5($this->input->post('password'));
            $response['data'] =$this->EmployerModel->InsertemployerData($data);
            if($response)
            {
                $this->session->set_flashdata('registered','User Registered Successfully');
                redirect('employer/login');exit;
            }
            else
            {
                $this->load->view('employer/templates/header1');
                $this->load->view('employer/signup');
                $this->load->view('employer/templates/footer1'); 
            }
        }
    }

    public function postJob()
    {
        if(!$this->session->userdata('id')){
        redirect('employer/login');exit;}
        $data['title']="Post New Job";
        $this->load->view('employer/templates/header2',$data);
        $this->load->view('employer/post_job');
        $this->load->view('employer/templates/footer1'); 
    }

    public function profile()
    {
        if(!$this->session->userdata('id')){
        redirect('employer/login');exit;}
        $result['title']= 'My Profile';
        $employer_id = $this->session->userdata('id');
        $result['employer'] = $this->EmployerModel->get_profile($employer_id);
        $this->load->view('employer/templates/header2',$result);
        $this->load->view('employer/profile',$result);
        $this->load->view('employer/templates/footer1');
    }

    public function editProfile()
    {
        if(!$this->session->userdata('id')){
        redirect('employer/login');exit;}
        $result['title']= 'Edit Profile';
        $employer_id = $this->session->userdata('id');
        $result['employer'] = $this->EmployerModel->get_profile($employer_id);
        $this->load->view('employer/templates/header2',$result);
        $this->load->view('employer/editProfile',$result);
        $this->load->view('employer/templates/footer1');
    }

    public function update()
    {
        if($this->input->post('submit'))
        $data['edit'] = $this->EmployerModel->get_profile($id); 
        $id = $this->session->userdata('id'); 
        $data['company_name']       = $this->input->post('company-name');
        $data['company_address']    = $this->input->post('company-address');
        $data['firstname']        	= $this->input->post('firstname');
        $data['lastname']         	= $this->input->post('lastname');
        $data['gender']            	= $this->input->post('gender');
        $data['contact_no']       	= $this->input->post('contact');
        $data['current_location']  	= $this->input->post('location');
        $data['email_id']           = $this->input->post('email');
        $update=$this->EmployerModel->updateEmployer($data, $id);
        if($update)
        {
            redirect('employer/profile');exit;
        }
        else
        {
            echo 'Edit did not work';
        }
    }

    public function insertJob()
    {
        $employer_id = $this->session->userdata('id');
        $data['employer_id']=$employer_id;
        $data['job_title']=$this->input->post('job-title');
        $data['description']=$this->input->post('description');
        $data['job_type']=$this->input->post('job-type');
        $data['job_location']=$this->input->post('location');
        $data['budget']=$this->input->post('budget');
        $data['experience']=$this->input->post('experience');

        $response=$this->EmployerModel->insertJobData($data);
        if($response)
        {
            $res['job']=$this->EmployerModel->myJobs($employer_id);
            redirect('employer/myJobs');exit;   
        }
        else
        {
            $data['title']="Post New Job";
            $this->load->view('employer/templates/header2',$data);
            $this->load->view('employer/post_job');
            $this->load->view('employer/templates/footer1'); 
        }    
    }

    public function myJobs()
    {
        if(!$this->session->userdata('id')){
        redirect('employer/login');exit;}
        $employer_id = $this->session->userdata('id');
        $res['myjobs']=$this->EmployerModel->myJobs($employer_id);
        if($res)
        {
            $this->load->view('employer/templates/header2');
            $this->load->view('employer/my_jobs',$res);
            $this->load->view('employer/templates/footer1');
        }
        else 
        {
            $this->session->set_flashdata('no_data','NO RECORDS FOUND'); 
        }
                
    }

    public function deleteJob($id)
    {
        $this->EmployerModel->deleteJob($id);
        redirect('employer/myJobs');exit;
    }

    public function editJob($id)
    {
        $res['edit']=$this->EmployerModel->editJob($id);
        $this->load->view('employer/templates/header2',$res);
        $this->load->view('employer/editJob',$res);
        $this->load->view('employer/templates/footer1');
    }

    public function updateJob($id)
    {
        $data = array(
        'job_title' => $this->input->post('job-title'),
        'description' => $this->input->post('description'),
        'budget' => $this->input->post('budget'),
        'job_type' => $this->input->post('job-type'),
        'job_location' => $this->input->post('location'),
        'experience' => $this->input->post('experience'));
        $this->EmployerModel->updateJob($id,$data);
        redirect('employer/myJobs');exit;
    }

    public function viewApplicants($job_id=0)
    {
        if(!$this->session->userdata('id')){
        redirect('employer/login');exit;}
        $candidate_details['applicant']=$this->CandidateModel->get_applicants($job_id);
        $this->load->view('employer/templates/header2');
        $this->load->view('employer/view_applicants',$candidate_details);
        $this->load->view('employer/templates/footer1');   
    }

    public function logout()
    {
        $this->session->sess_destroy();
        redirect('employer/login'); exit;
    }

    public function download_resume($id)
    {
        $result = $this->EmployerModel->get_resume($id);
        $fileInfo = $result[0];
        $resume = 'uploads/'.$fileInfo['resume'];
        force_download($resume, NULL);
        print_r($fileInfo);
    }     
}