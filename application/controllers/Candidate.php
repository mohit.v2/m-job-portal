<?php
class Candidate extends CI_Controller 
{
    public function __construct()
    {
        parent::__construct();
        $this->load->database();
        $this->load->model('CandidateModel');
        $this->load->model('EmployerModel');
    }

    public function login()
    {
        if($this->session->userdata('id')){
        redirect('candidate/dashboard'); exit;}
        $data['title'] = 'Candidate Log In';
        $this->form_validation->set_rules('email','Email ID','trim|required|valid_email');
        $this->form_validation->set_rules('password','Password','required');
        $this->form_validation->set_error_delimiters('<div class="error">', '</div>');
        if($this->form_validation->run() === FALSE)
        {
            $this->load->view('candidate/templates/header1',$data);
            $this->load->view('candidate/login',$data);
            $this->load->view('candidate/templates/footer0');  
        }
        else
        {
            $email = $this->input->post('email');
            $password = md5($this->input->post('password'));
            $candidate_id	=$this->CandidateModel->loginCandidate($email,$password);
            if($candidate_id)
            {
                $candidate  	=array(
                'id'        	=>$candidate_id,
                'username'      =>$email);
                $this->session->set_userdata($candidate);
                $this->session->set_flashdata('user_loggedin','You are now logged in');
                redirect('candidate/dashboard',$candidate);
            }
            else
            {
                $this->session->set_flashdata('login_failed','Incorrect Username or Password');
                redirect('candidate/login');
            } 
        }
    }

    public function signup()
    {
        if($this->session->userdata('id')){
            redirect('candidate/dashboard');exit;}

        $this->form_validation->set_rules('firstName','First Name','required');
        $this->form_validation->set_rules('email','Email Id','trim|required|valid_email|callback_check_email_exists');
        $this->form_validation->set_rules('lastname','Last name','required');
        $this->form_validation->set_rules('password','Password','required');
        $this->form_validation->set_rules('passconf', 'Password Confirmation', 'required|matches[password]');
        $this->form_validation->set_rules('job_title','Job Title','required');
        $this->form_validation->set_rules('checkbox','terms and conditions','required');
        $this->form_validation->set_rules('profile_pic','Profile Picture','required');
        $this->form_validation->set_rules('contact','Contact No','required|callback_check_contact_exists');

        if($this->form_validation->run()=== FALSE)
        {
            $this->load->view('candidate/templates/header1');
            $this->load->view('candidate/signup');
            $this->load->view('candidate/templates/footer0');
        }
        else 
        {
            $data['profile_pic']	    = $this->input->post('profile_pic');
            $data['resume']				= $this->input->post('resume');
            $data['job_title']			= $this->input->post('job_title');
            $data['current_location']	= $this->input->post('location');
            $data['current_ctc']		= $this->input->post('ctc');
            $data['firstname']			= $this->input->post('firstName');
            $data['lastname']			= $this->input->post('lastname');
            $data['gender']				= $this->input->post('gender');
            $data['dob']				= $this->input->post('dob');
            $data['contact_no']			= $this->input->post('contact');
            $data['email_id']			= $this->input->post('email');
            $data['password']			= md5($this->input->post('password'));
            $data['experience']         = $this->input->post('experience');
            $response['data']			= $this->CandidateModel->insertCandidate($data);
            if($response)
            {
                $this->session->set_flashdata('registered','User Registered Successfully');
                redirect('candidate/login'); exit; 
            }
            else
            {
                $this->session->set_flashdata('register_fail','User Registration Unsuccessful');
                $this->load->view('candidate/templates/header1');
                $this->load->view('candidate/signup');
                $this->load->view('candidate/templates/footer0');
            }
        }
    }

    public function contactUs()
    {
        $data['title'] = 'Contact Us';
        $this->load->view('candidate/templates/header1',$data);
        $this->load->view('candidate/contactus');
        $this->load->view('candidate/templates/footer0');
    }

    public function check_email_exists($email)
    {
        $this->form_validation->set_message('check_email_exists','That email is taken please choose a different one');
        if( $this->CandidateModel->check_email_exists($email))
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    public function check_contact_exists($contact)
    {
        $this->form_validation->set_message('check_contact_exists','Phone number already exists, please login or choose a different number for registraion');
        if( $this->CandidateModel->check_contact_exists($contact))
        {
            return true;
        }
        else
        {
            return false;
        }
    }
    
    public function dashboard()
    {  
        if(!$this->session->userdata('id')){
        redirect('candidate/login');exit;}
        $data['title'] = 'Dashboard';
        $this->load->view('candidate/templates/header2',$data);
        $this->load->view('candidate/dashboard',$data);
        $this->load->view('candidate/templates/footer1');         
    }
	
    public function logout()
    {
        $this->session->sess_destroy();
        return redirect('candidate/login');exit;
    }
   
    public function profile()
    {
        if(!$this->session->userdata('id')){
        redirect('candidate/login');exit;}
        $result['title']= 'My Profile';
        $candidate_id = $this->session->userdata('id');
        $result['candidate'] = $this->CandidateModel->get_profile($candidate_id);
        $this->load->view('candidate/templates/header2',$result);
        $this->load->view('candidate/profile',$result);
        $this->load->view('candidate/templates/footer1');            
    }  
                
    public function editProfile()
    {
        if(!$this->session->userdata('id')){
        redirect('candidate/login');exit;}
        $result['title']= 'Edit Profile';
        $id = $this->session->userdata('id');
        $result['candidate'] = $this->CandidateModel->get_profile($id);
        $this->load->view('candidate/templates/header2',$result);
        $this->load->view('candidate/edit_profile',$result);
        $this->load->view('candidate/templates/footer1');
    }
    
    public function update()
    {
        if($this->input->post('submit'))
        $data['edit'] = $this->CandidateModel->get_profile($id);
        $id = $this->session->userdata('id');
        $data['profile_pic']       	= $this->input->post('profile_pic');
        $data['resume']            	= $this->input->post('resume');
        $data['job_title']         	= $this->input->post('job_title');
        $data['experience']        	= $this->input->post('experience');
        $data['current_ctc']       	= $this->input->post('ctc');
        $data['firstname']        	= $this->input->post('firstName');
        $data['lastname']         	= $this->input->post('lastname');
        $data['gender']            	= $this->input->post('gender');
        $data['dob']               	= $this->input->post('dob');
        $data['contact_no']       	= $this->input->post('contact');
        $data['current_location']  	= $this->input->post('location');
        $data['email_id']           = $this->input->post('email');
        $update=$this->CandidateModel->update($data, $id);
        if($update)
        {
            redirect('candidate/profile');
            exit;
        }
        else
        {
            echo 'Edit did not work';
        }    
    }
   
    public function search()
    {
        if(!$this->session->userdata('id')){
        redirect('candidate/login');exit;}
        $jobtype = $this->input->post('job-type');
        $location = $this->input->post('job-location');   
       
        $result['job'] = $this->EmployerModel->search_job($jobtype,$location);
        if($result)
        {
        $this->load->view('candidate/templates/header2');
        $this->load->view('candidate/search',$result);
        $this->load->view('candidate/templates/footer1');
        }
        else
        {
        echo "Search Result Not Found";
        }   
    }

    public function searchJobs()
    {
        if(!$this->session->userdata('id')){
        redirect('candidate/login');exit;}
        $result['job'] = $this->EmployerModel->displayOnSearch();
        $result['title']= 'Job Search';
        $this->load->view('candidate/templates/header2',$result);
        $this->load->view('candidate/search',$result);
        $this->load->view('candidate/templates/footer1');
    }

    public function jobDetails($id)
    {
       if(!$this->session->userdata('id')){
        redirect('candidate/login');exit;}
        $res['title'] = 'Job Details';
        $res['job']=$this->EmployerModel->getJobById($id);
        $this->load->view('candidate/templates/header2',$res);
        $this->load->view('candidate/job_details',$res);
        $this->load->view('candidate/templates/footer1');
    }

    public function appliedJobs($id)
    {  
        if(!$this->session->userdata('id')){
        redirect('candidate/login');exit;}
        $result['data'] = $this->EmployerModel->getJobById($id);
        $candidate_id = $this->session->userdata('id');
        $employer_id = $result['data']['employer_id'];
        $job_id = $result['data']['id'];
        $company_name = $result['data']['company_name'];
        $current_location = $result['data']['current_location'];
        $exp = $result['data']['experience'];
        $data = array(
            'job_id'           => $job_id,
            'company_name'     => $company_name,
            'current_location' => $current_location,
            'min_exp'          => $exp,
            'candidate_id'     => $candidate_id,
            'employer_id'      => $employer_id);
        $appliedjobs = $this->CandidateModel->insertAppliedJobs( $candidate_id,$job_id,$data); 
        redirect('candidate/appliedJobDetails');exit;    
    }

    public function appliedJobDetails()
    {
        if(!$this->session->userdata('id')){
        redirect('candidate/login');exit;}
        $candidate_id = $this->session->userdata('id');
        $result['title']= 'My Applied Jobs';
        $result['appliedjob'] = $this->CandidateModel->displayAppliedJobs($candidate_id);
        $this->load->view('candidate/templates/header2',$result);
        $this->load->view('candidate/applied_jobs',$result);
        $this->load->view('candidate/templates/footer1');
    }

    public function withdrawApplication($id)
    {
        if(!$this->session->userdata('id')){
        redirect('candidate/login');exit;}
        $res['job']=$this->EmployerModel->getJobById($id);
        $this->load->view('candidate/templates/header2');
        $this->load->view('candidate/withdraw_application',$res);
        $this->load->view('candidate/templates/footer1');
    }

    public function deleteAppliedJob($job_id)
    {
        $candidate_id = $this->session->userdata('id');
        $this->CandidateModel->deleteAppliedJob($job_id,$candidate_id);
        redirect('candidate/appliedJobDetails');exit;   
    }

    public function download_profile($id)
    {
		$this->load->helper('download');
		$result = $this->EmployerModel->download_picture($id);
        $fileInfo = $result[0];
        $resume = 'uploads/'.$fileInfo['profile_pic'];
        force_download($resume, NULL);
	}  

}
